<div style="text-align:center"><img src="activos/imagenes/V-13+Tatuaje.jpeg" width="613" height="613" /></div>

# <center>Viernes 13 [<font color="red">*V-13*</font>]</center>

## Introducción

> El "Viernes 13" es una superstición popular que ha existido durante siglos. Se cree que es un día de mala suerte, donde ocurren eventos desafortunados. Se desconoce exactamente cómo se originó esta superstición, pero se han propuesto varias teorías.

> Una de las teorías más populares se remonta a la Última Cena, donde Jesús y sus doce apóstoles se sentaron a la mesa, haciendo un total de 13 personas presentes. Se dice que uno de los apóstoles presentes, Judas, traicionó a Jesús, lo que llevó a su crucifixión. De esta manera, se cree que el número 13 se convirtió en sinónimo de mala suerte.

<div style="text-align:center"><img src="activos/imagenes/V-13+Jason.jpeg" width="368" height="433" /></div>

> Otra teoría sugiere que la superstición del "Viernes 13" se originó en la Edad Media. El viernes era considerado un día de mala suerte, ya que se creía que era el día en que Adán y Eva comieron el fruto prohibido en el Jardín del Edén. Además, en la mitología nórdica, el número 13 es considerado un número desafortunado. La combinación del viernes y el número 13 se consideraba, por tanto, especialmente desafortunada.

> La superstición del "Viernes 13" se ha perpetuado a lo largo de los años y ha sido objeto de muchas películas y libros de terror. Aunque no hay evidencia sólida de que el "Viernes 13" sea un día más peligroso que cualquier otro día, muchas personas todavía sienten un miedo irracional hacia él. De hecho, algunos hoteles y edificios omiten el piso número 13 y algunas aerolíneas evitan programar vuelos con el número 13.

<div style="text-align:center"><img src="activos/imagenes/V-13+Beisbol.png" width="383" height="383" /></div>

> En definitiva, el "Viernes 13" es una superstición que ha existido durante siglos. Aunque no hay evidencia concreta de que este día sea más peligroso que cualquier otro, muchas personas siguen creyendo en su mala suerte. Sin embargo, es importante recordar que la superstición no es algo que deba ser tomado en serio y que es importante no dejar que el miedo nos controle.

## Desarrollo

### <font color="red">*V-13*</font> en la historia

#### Los 13 años de la historia que han tenido más <font color="red">*V-13*</font>

<div style="text-align:center"><img src="activos/imagenes/V-13+Genial.jpg" width="310" height="310" /></div>

> - 2012 - 3 viernes 13: enero, abril y julio.
> 
> - 1970 - 3 viernes 13: febrero, marzo, noviembre.
> 
> - 1938 - 3 viernes 13: abril, mayo y junio.
> 
> - 2023 - 2 viernes 13: enero y octubre.
> 
> - 2021 - 2 viernes 13: enero y agosto.
> 
> - 2000 - 2 viernes 13: octubre y diciembre.
> 
> - 1996 - 2 viernes 13: septiembre y diciembre.
> 
> - 1987 - 2 viernes 13: febrero y marzo.
> 
> - 1981 - 2 viernes 13: febrero y marzo.
> 
> - 1964 - 2 viernes 13: marzo y noviembre.
> 
> - 1955 - 2 viernes 13: mayo y noviembre.
> 
> - 1949 - 2 viernes 13: mayo y noviembre.
> 
> - 1933 - 2 viernes 13: enero y octubre.

> Cabe destacar que cada año tiene al menos un viernes 13, pero en algunos años pueden haber más de uno.

#### Los 13 acontecimientos más relevantes presentes en la historia de la humanidad sucedidos un <font color="red">*V-13*</font>

<div style="text-align:center"><img src="activos/imagenes/V-13+Suerte.jpeg" width="438" height="240" /></div>

> Es importante tener en cuenta que esta lista es solo una selección y que hay muchos otros eventos históricos que también tuvieron lugar en un viernes 13. Cabe destacar que sólo algunos de los eventos ocurridos en un viernes 13 fueron necesariamente negativos o trágicos, algunos simplemente fueron notables o curiosos.

> 1. 13 de octubre de 1307: El rey Felipe IV de Francia ordenó la detención y ejecución de los Caballeros Templarios.
> 
> 1. 13 de agosto de 1521: El conquistador español Hernán Cortés capturó la ciudad de Tenochtitlán, poniendo fin al Imperio Azteca.
> 
> 1. 13 de octubre de 1303: El papa Bonifacio VIII fue arrestado por orden del rey Felipe IV de Francia.
> 
> 1. 13 de septiembre de 1940: Durante la Segunda Guerra Mundial, los bombardeos alemanes sobre Londres mataron a más de 300 personas.
> 
> 1. 13 de mayo de 1955: La Unión Soviética y los países del Pacto de Varsovia firmaron un tratado de amistad y cooperación en Europa del Este.
> 
> 1. 13 de marzo de 1781: William Herschel descubrió Urano, el séptimo planeta del sistema solar.
> 
> 1. 13 de junio de 1952: La Reina Isabel II fue coronada en la Abadía de Westminster en Londres, Inglaterra.
> 
> 1. 13 de enero de 1939: Los incendios forestales en Australia destruyeron alrededor de 20,000 km² de bosques.
> 
> 1. 13 de octubre de 1972: Un avión de la Fuerza Aérea Uruguaya se estrelló en los Andes, dando lugar al infame caso de los Andes, donde los sobrevivientes tuvieron que recurrir al canibalismo para sobrevivir.
> 
> 1. 13 de enero de 2012: El crucero Costa Concordia se hundió cerca de la isla italiana de Giglio, dejando 32 personas muertas.
> 
> 1. 13 de octubre de 1972: El accidente del vuelo 571 de la Fuerza Aérea Uruguaya en 1972, que dejó a 16 sobrevivientes después de pasar 72 días en los Andes.
> 
> 1. 13 de septiembre de 2013: El ciclón Man-yi causó graves inundaciones en el oeste de Japón, matando a 35 personas.
> 
> 1. 13 de mayo de 1981: El Papa Juan Pablo II fue herido gravemente en un intento de asesinato en la Plaza de San Pedro en el Vaticano.

<div style="text-align:center"><img src="activos/imagenes/V-13+.jpeg" width="306" height="267" /></div>

> Sin embargo, de los 13 eventos mencionados anteriormente, se podría decir que 7 de ellos tienen un impacto negativo, mientras que los otros 6 tienen un impacto neutral o positivo. Esto da lugar a un porcentaje aproximado del 54% de eventos con impacto negativo y un 46% de eventos con impacto neutral o positivo.

> Es importante tener en cuenta que la clasificación de los hechos en positivos, negativos o neutrales es subjetiva y puede variar según la interpretación de cada persona. Además, los hechos pueden tener diferentes niveles de impacto dependiendo de cómo se miren y de las perspectivas de las personas involucradas.

#### La relación del <font color="red">*V-13*</font> y el martes 13

<div style="text-align:center"><img src="activos/imagenes/V-13+Excelente.jpg" width="334" height="334" /></div>


> Existe una cierta superstición en torno a los días 13, especialmente cuando caen en viernes o martes. El viernes 13 es considerado por muchas personas como un día de mala suerte, mientras que el martes 13, aunque menos conocido, también tiene su propia reputación de ser un día propicio para el infortunio.

> Aunque ambos días son considerados por algunos como desafortunados, sus orígenes son diferentes. El viernes 13 tiene su origen en la mitología nórdica, donde se cree que el número 13 es un número de mala suerte y el viernes es el día en que se llevaron a cabo las ejecuciones y otras formas de castigo. En la cultura occidental, esta superstición se ha extendido y se ha convertido en una creencia popular que aún perdura hoy en día.

<div style="text-align:center"><img src="activos/imagenes/V-13+Felz.jpg" width="287" height="287" /></div>

> Por otro lado, el martes 13 tiene su origen en la religión cristiana. Se cree que la mala suerte asociada con este día se remonta a la Última Cena, cuando Jesús se reunió con sus doce apóstoles, incluyendo a Judas, el traidor que más tarde lo entregó a las autoridades. En algunas culturas, el martes es considerado el día más desafortunado de la semana, lo que aumenta la creencia en la mala suerte del martes 13.

> Aunque los días 13 no son diferentes a cualquier otro día del año en términos de su influencia en los acontecimientos, muchos creen en la superstición de que estos días son más propensos a traer desgracias y problemas. En algunos casos, esta creencia ha llevado a personas a evitar hacer ciertas cosas en estos días, como viajar, casarse, firmar contratos o hacer inversiones financieras importantes.

<div style="text-align:center"><img src="activos/imagenes/V-13+Maravilloso.jpeg" width="348" height="378" /></div>

> Finalmente, la superstición de los días 13 es un fenómeno cultural que ha persistido a lo largo de los siglos. Tanto el viernes 13 como el martes 13 tienen sus orígenes en la mitología y la religión, y se han convertido en una creencia popular que todavía se mantiene hoy en día. Aunque no hay ninguna evidencia científica que respalde estas creencias, muchas personas continúan creyendo en la mala suerte asociada con estos días, lo que a veces influye en sus decisiones y acciones.

### Mejorar la percepción del <font color="red">*V-13*</font>

<div style="text-align:center"><img src="activos/imagenes/V-13+Godinez.jpg" width="310" height="310" /></div>

> - Cambia tu perspectiva: En lugar de pensar en el viernes 13 como un día de mala suerte, trata de enfocarte en los aspectos positivos del día. Por ejemplo, piensa en la oportunidad de tener un día de descanso o de hacer algo especial.
> 
> - Realiza actividades relajantes: Practicar la meditación, yoga o simplemente disfrutar de un baño caliente, son actividades que te ayudarán a reducir el estrés y la ansiedad, permitiéndote enfrentar el día de manera más tranquila.
> 
> - Haz algo que te guste: Aprovecha el día para hacer algo que te guste, como leer un libro, ver una película, escuchar música, hacer deporte, cocinar algo especial, entre otras opciones.
> 
> - Organiza un evento: Invita a tus amigos o familiares a realizar alguna actividad juntos, como una cena o una reunión, lo que te ayudará a distraerte y disfrutar de un momento agradable en compañía de las personas que te importan.
> 
> - Evita las supersticiones: En lugar de creer en supersticiones, enfócate en el presente y en lo que está sucediendo en tu vida en ese momento. Recuerda que eres dueño de tus decisiones y que puedes decidir cómo vivir y enfrentar cada día, independientemente de la fecha.

## Conclusión


<div style="text-align:center"><img src="activos/imagenes/V-13+Chiste.png" width="284" height="272" /></div>

> En resumen, aunque el Viernes 13 ha sido tradicionalmente considerado un día de mala suerte, no hay nada en la ciencia o en la razón para respaldar esa creencia. En última instancia, nuestra percepción de la realidad está influenciada por nuestros pensamientos y emociones. Si decidimos enfocarnos en lo positivo, podemos cambiar nuestra percepción del mundo que nos rodea, incluso en un día tan temido como el Viernes 13. Así que, celebremos la vida con alegría, esperanza y optimismo, y dejemos que la mala suerte se desvanezca en el pasado.

### Reflexión Final

<div style="text-align:center"><img src="activos/imagenes/V-13+Gato.jpeg" width="322" height="322" /></div>

<p style="text-align:center;color:blue">
Viernes 13, día de la suerte,<br>
una oportunidad de renacer,<br>
enfoca tu mente en lo positivo,<br>
y verás el mundo florecer.<br>
<br>
En lugar de temer el Viernes 13,<br>
abramos nuestras mentes a la suerte,<br>
centrémonos en la felicidad y la bondad,<br>
y veremos que la vida se hace más fuerte.<br>
<br>
La historia nos muestra el bien y el mal,<br>
pero siempre hay una oportunidad para cambiar,<br>
así que celebremos este día con una sonrisa,<br>
y dejemos que la alegría nos haga bailar.
</p>

<div style="text-align:center"><img src="activos/imagenes/V-13+Amor.jpeg" width="326" height="109" /></div>
